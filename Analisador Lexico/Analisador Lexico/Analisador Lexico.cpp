// Analisador Lexico.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "ctype.h"
#include <fstream>
#include <iostream>
#include <conio.h>
using namespace std;

ifstream archivo("temporal.cpp");//Abre el archivo
ofstream mensajes("error.txt");//Archivo para los mensajes de error

struct nodo
{
	nodo *sig;
	nodo *ant;
	char token[50];
}*inicio, *aux, *aux2;

struct var
{
	var *sig;
	var *ant;
	char token[50];
}*iniciov, *auxv, *aux2v;

char *PalaReser[20] = { "out", "in", "if", "ifnot", "}", "while", "for", "do", "fun" };//Palabras reservadas
char *OpeArti[20] = { "+", "-", "*", "/" };//Operadores aritemeticos
char *OpeLogi[20] = { "==", "!=", "<", ">", "<=", ">=" };//Operadores logicos
char *TipoVar[20] = { "int", "chr", "flt" };//Tipos de variables
char *Ciclos[20] = { "if", "while", "for", "fn", "do" };//Tipos de ciclo
char *CiclosEnd[20] = { "}", "}", "}", "}", "}" };//Terminos de ciclos

void inicia();//Inicia el encabezado de la lista
void lexico();//Primera etapa
void lexico2();//Almacena variables o funciones
void pura();//verifica que sean puras letras
void termina();//verifica si termino el if, while, for, do, funcion
int comentarios(char[], int, int);
void mostrar();//Muestra los tokens
void sintatico();//Segunda etapa
void analizariguales();


int _tmain(int argc, _TCHAR* argv[])
{
	inicia();
	lexico();
	lexico2();
	analizariguales();
	mostrar();
	system("pause>nul");
	return 0;
}


void inicia()
{
	inicio = new nodo;
	inicio->sig = inicio;
	inicio->ant = inicio;
	strcpy_s(inicio->token, "");
	iniciov = new var;
	iniciov->sig = iniciov;
	iniciov->ant = iniciov;
	strcpy_s(iniciov->token, "");
}
void lexico()
{
	char copia[256], temp[50];
	if (archivo.fail())//Verifica si existe
	{
		cout << "Archivo no encontrado " << endl;
	}
	else
	{
		int i = 0, cont = 0, linea = 0, error;
		while (archivo.eof() == 0)//Si no es el final
		{
			cont = 0, i = 0;
			linea++;
			archivo.getline(copia, 255);//Consigue la primera linea
			copia[strlen(copia)] = '\0';
			while (copia[i] != '\0')
			{
				cont = 0;
				while (copia[i] != 32 && copia[i] != '\0')
				{
					temp[cont] = copia[i];
					i++;
					cont++;
				}
				while (copia[i] == ' ')
				{
					i++;
				}
				temp[cont] = '\0';
				error = comentarios(temp, cont, linea);
				if (error == 0)
				{
					mensajes << "--Error en la linea: " << linea << " '" << temp << "'" << endl;
				}
				else if (error != 2)
				{
					if (inicio->sig == inicio)
					{
						aux = new nodo;
						aux->sig = inicio;
						aux->ant = inicio;
						for (int a = 0; a < cont; a++)
						{
							aux->token[a] = temp[a];
						}
						aux->token[cont] = '\0';
						inicio->sig = aux;
						inicio->ant = aux;
					}
					else
					{
						aux2 = aux;
						aux = new nodo;
						aux->sig = inicio;
						aux->ant = aux2;
						aux2->sig = aux;
						inicio->ant = aux;
						for (int a = 0; a < cont; a++)
						{
							aux->token[a] = temp[a];
						}
						aux->token[cont] = '\0';
					}
				}
			}
		}
	}
	cout << "Analisis lexico completado " << endl;
}


void mostrar()
{
	aux = inicio->sig;
	while (aux->sig != inicio)
	{
		cout << aux->token << ",";
		aux = aux->sig;
	}
	cout << aux->token << ",";
}

int comentarios(char a[], int size, int linea)
{
	int i = 0;
	while (i < size)
	{
		if (a[i] == '/')
		{
			if (a[i + 1] == '/')
			{
				return 2;
			}
			else
			{
				return 0;
			}
		}
		i++;
	}
	return 1;
}

void sintatico()
{

}

void lexico2()
{
	aux = inicio->sig;
	int i = 0, si = 0, error, j = 0, si2 = 0;
	while (aux->sig != inicio)
	{
		i = 0, j = 0;
		while (i < 3)
		{
			if (si == 1)
			{
				pura();
				si = 0;
			}
			if (strcmp(TipoVar[i], aux->token) == 0)
			{
				si = 1;
			}
			i++;
		}
		if (si == 1)
		{
			pura();
			si = 0;
		}
		while (j < 5)
		{
			if (si2 == 1)
			{
				termina();
				si2 = 0;
			}
			if (strcmp(Ciclos[i], aux->token) == 0)
			{
				si2 = 1;
			}
			j++;
		}
		if (si2 == 1)
		{
			termina();
			si2 = 0;
		}
		aux = aux->sig;
	}
}

void pura()
{
	int i = 0, size, ok = 0;
	aux2 = aux->sig;
	size = strlen(aux2->token);
	while (i < size)
	{
		if ((aux2->token[i]>96 && aux2->token[i]<123))
		{
			ok++;
		}
		i++;
	}
	if (ok != size)
	{
		mensajes << "--Error de sintaxis: " << aux2->token << endl;
	}
	else
	{
		int i = 0, j = 0, ok;
		auxv = iniciov->sig;
		while (auxv->sig != iniciov)
		{
			i++;
			auxv = auxv->sig;
			if (strcmp(aux2->token, auxv->token) == 1)
			{
				j++;
			}
		}
		i++;
		if (strcmp(aux2->token, auxv->token) == 1)
		{
			j++;
		}
		if (i == j)
		{
			if (iniciov->sig == iniciov)
			{
				auxv = new var;
				auxv->sig = iniciov;
				auxv->ant = iniciov;
				strcpy_s(auxv->token, aux2->token);
				iniciov->sig = auxv;
				iniciov->ant = auxv;
			}
			else
			{
				aux2v = auxv;
				auxv = new var;
				auxv->sig = iniciov;
				auxv->ant = aux2v;
				aux2v->sig = auxv;
				iniciov->ant = auxv;
				strcpy_s(auxv->token, aux2->token);
			}
		}
		else
		{
			mensajes << "--Error variable: '" << auxv->token << "' repetida" << endl;
		}
	}
}
void termina()
{
	aux2 = aux->sig;
	int i = 0, j = 0, k = 0;
	while (aux2->sig != inicio)
	{
		while (i < 5)
		{
			if (strcmp(aux2->token, CiclosEnd[i]) == 1)
			{
				j++;
			}
			i++;
			k++;
		}
		i = 0;
		aux2 = aux2->sig;
	}
	while (i < 5)
	{
		if (strcmp(aux2->token, CiclosEnd[i]) == 1)
		{
			j++;
		}
		i++;
		k++;
	}
	if (i == j)
	{
		mensajes << "--Error cerrar el ciclo: " << aux->token << endl;
	}
}


void analizariguales()
{
	aux = inicio->sig;
	aux2 = inicio->sig;
	while (aux->sig != inicio)
	{
		while (aux2->sig != inicio)
		{
			if (strcmp(aux->token, aux2->token) == 0)
			{
				mensajes << "--Error de sintaxis el nombre: " << aux->token << "ya se esta usando" << endl;
			}
			aux2 = aux2->sig;
		}
		aux = aux->sig;
	}
	if (strcmp(aux->token, aux2->token) == 0)
	{
		mensajes << "--Error de sintaxis el nombre: " << aux->token << " ya se esta usando" << endl;
	}
}