﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace IDE_Compilar
{
    public partial class Form1 : Form
    {
        class variables
        {
            public static int guardar = 0;//**Si se altera el codigo se procedera a cambiar 1: para guardar
            public static int guardarp = 1;//**Esta variable controla la primera vez de guardado.
            //**1 es igual a guardar por primera vez y guardar la direccion en direccion
            //**0 indica que ya tiene la direccion del archivo
            public static string direccion = "1001";//**Aqui se almacena la direccion del archivo
            public static string nombre = " - IDE";//**Nombre del programa se utiliza para poner el titulo del Form
            public static string nombrea = "Sin titulo";//**Nombre del archivo se pone en el titulo del Form
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codigo.Visible = true;
            /*Crea un archivo nuevo*/
            if (variables.guardar == 0)//Si el archivo que tiene ya esta guardado
            {
                codigo.Clear();//Se limpia el textBox
                codigo.Focus();//Se establece el cursor
                variables.guardarp = 1;//Se modifica para que guarde por primera vez
                variables.guardar = 0;//Se quita el guardado
                variables.nombrea = "Sin titulo";//Se coloca sin titulo en el Form
                Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Se coloca el titulo
            }
            else//Si no esta guardado
            {
                var resultado = MessageBox.Show("Desea guardar cambios en " + variables.nombrea, "Codigo no guardado",
                    MessageBoxButtons.YesNoCancel);//Mensaje para guardar cambios
                if (resultado == DialogResult.Yes)//Si presiona "SI"
                {
                    SaveFileDialog saveFile2 = new SaveFileDialog();//Se crea una variable para el dialogo
                    saveFile2.DefaultExt = "*.gg";//Se declara el tipo de archivo por defecto
                    saveFile2.Filter = "Archivos (.*gg)|*.gg|Todos los archivos (*.*)|*.*";//Flitro de busqueda
                    saveFile2.Title = "Guardar";//Titulo del dialogo
                    if (variables.guardarp == 1)//Si se guarda por primera vez
                    {
                        if (saveFile2.ShowDialog() == DialogResult.OK && saveFile2.FileName.Length > 0)//Si presiono guardar
                        {
                            StreamWriter savep = File.CreateText(saveFile2.FileName);//se crea una variable para crear un archivo
                            savep.Write(codigo.Text);//Se incorpora el codigo hecho en el programa
                            savep.Flush();//Se refresca
                            savep.Close();//Se cierra
                            variables.direccion = saveFile2.FileName;//Se guarda la direccion elegida
                            variables.guardarp = 0;//Se modifica el guardado por primera vez
                        }
                    }
                    else//Si ya habia guardado por primera vez o abierto el archivo
                    {
                        StreamWriter savep = File.CreateText(variables.direccion);//Crea el archivo con la direccion existente
                        savep.Write(codigo.Text);//Se incorpora el codigo
                        savep.Flush();//Se refresca
                        savep.Close();//Se cierra
                    }
                    variables.nombrea = "Sin titulo";//Se coloca sin titulo en el Form
                    Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Se coloca sin titulo
                    codigo.Text = "";//Se limpia el textBox
                    codigo.Focus();//Se establece el cursor
                    variables.guardarp = 1;//Se modifica para que guarde por primera vez
                    variables.guardar = 0;//Se quita el guardado
                }
                if (resultado == DialogResult.No)//Si preciono NO
                {
                    variables.nombrea = "Sin titulo";//Se coloca sin titulo en el Form
                    Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Se coloca sin titulo
                    codigo.Text = "";//Se limpia el textBox
                    codigo.Focus();//Se establece el cursor
                    variables.guardarp = 1;//Se modifica para que guarde por primera vez
                    variables.guardar = 0;//Se quita el guardado
                }
            }
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Abre un archivo*/
            codigo.Visible = true;
            OpenFileDialog abrir = new OpenFileDialog();//Se crea una variable para el dialogo
            abrir.DefaultExt = "*.gg";//Se declara el tipo de archivo por defecto
            abrir.Filter = "Archivos (.*gg)|*.gg|Archivos (.*cpp)|*.cpp|Todos los archivos (*.*)|*.*";//Flitro de busqueda
            abrir.Title = "Abrir un archivo";//Titulo del dialogo
            if (variables.guardar == 1)//Si el archivo que tiene no esta guardado
            {
                var resultado = MessageBox.Show("Desea guardar cambios en " + variables.nombrea, "IDE",
                    MessageBoxButtons.YesNoCancel);//Mensaje para guardar cambios
                if (resultado == DialogResult.Yes)//Si presiona "SI"
                {
                    SaveFileDialog saveFile2 = new SaveFileDialog();//Se crea una variable para el dialogo
                    saveFile2.DefaultExt = "*.gg";//Se declara el tipo de archivo por defecto
                    saveFile2.Filter = "Archivos (.*gg)|*.gg|Archivos (.*cpp)|*.cpp|Todos los archivos (*.*)|*.*";//Flitro de busqueda
                    saveFile2.Title = "Guardar";//Titulo del dialogo
                    if (variables.guardarp == 1)//Si se guarda por primera vez
                    {
                        if (saveFile2.ShowDialog() == DialogResult.OK && saveFile2.FileName.Length > 0)//Si presiono guardar
                        {
                            StreamWriter savep = File.CreateText(saveFile2.FileName);//se crea una variable para crear un archivo
                            savep.Write(codigo.Text);//Se incorpora el codigo hecho en el programa
                            savep.Flush();//Se refresca
                            savep.Close();//Se cierra
                            variables.direccion = saveFile2.FileName;//Se guarda la direccion elegida
                            variables.guardarp = 0;//Se modifica el guardado por primera vez
                        }
                    }
                    else//Si ya habia guardado por primera vez o abierto el archivo
                    {
                        StreamWriter savep = File.CreateText(variables.direccion);//Crea el archivo con la direccion existente
                        savep.Write(codigo.Text);//Se incorpora el codigo
                        savep.Flush();//Se refresca
                        savep.Close();//Se cierra
                    }
                    if (abrir.ShowDialog() == DialogResult.OK && abrir.FileName.Length > 0)//Si presiono abrir
                    {
                        StreamReader leer = File.OpenText(abrir.FileName);//Crea el archivo con la direccion existente
                        string contenido = null;//Se crea una cadena
                        contenido = leer.ReadToEnd();//Se le pasa el contenido del archivo a a la cadena
                        leer.Close();//Se cierra
                        codigo.Text = contenido.ToString();//Se le pasa a el textBox
                        codigo.Select(codigo.Text.Length, 0);//Se pone el cursor en el final
                        variables.nombrea = Path.GetFileNameWithoutExtension(abrir.FileName);//Nombre del archivo
                        variables.direccion = abrir.FileName;//Se declara la direccion del archivo
                        variables.guardarp = 0;//Se define que ya tiene la direccion
                        variables.guardar = 0;//No se debe guardar porque no hay cambios
                        Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Se declara el nombre en el titulo del Form
                    }
                }
                if (resultado == DialogResult.No)//Si preciono no
                {
                    if (abrir.ShowDialog() == DialogResult.OK && abrir.FileName.Length > 0)//Si presiono abrir
                    {
                        StreamReader leer = File.OpenText(abrir.FileName);//Crea el archivo con la direccion existente
                        string contenido = null;//Se crea una cadena
                        contenido = leer.ReadToEnd();//Se le pasa el contenido del archivo a a la cadena
                        leer.Close();//Se cierra
                        codigo.Text = contenido.ToString();//Se le pasa a el textBox
                        codigo.Select(codigo.Text.Length, 0);//Se pone el cursor en el final
                        variables.nombrea = Path.GetFileNameWithoutExtension(abrir.FileName);//Nombre del archivo
                        variables.direccion = abrir.FileName;//Se declara la direccion del archivo
                        variables.guardarp = 0;//Se define que ya tiene la direccion
                        variables.guardar = 0;//No se debe guardar porque no hay cambios
                        Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Se declara el nombre en el titulo del Form
                    }
                }
            }
            else//Si ya estaba guardado
            {
                if (abrir.ShowDialog() == DialogResult.OK && abrir.FileName.Length > 0)//Si presiono abrir
                {
                    StreamReader leer = File.OpenText(abrir.FileName);//Crea el archivo con la direccion existente
                    string contenido = null;//Se crea una cadena
                    contenido = leer.ReadToEnd();//Se le pasa el contenido del archivo a a la cadena
                    leer.Close();//Se cierra
                    codigo.Text = contenido.ToString();//Se le pasa a el textBox
                    codigo.Select(codigo.Text.Length, 0);//Se pone el cursor en el final
                    variables.nombrea = Path.GetFileNameWithoutExtension(abrir.FileName);//Nombre del archivo
                    variables.direccion = abrir.FileName;//Se declara la direccion del archivo
                    variables.guardarp = 0;//Se define que ya tiene la direccion
                    variables.guardar = 0;//No se debe guardar porque no hay cambios
                    Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Se declara el nombre en el titulo del Form
                }
            }
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Guarda el archivo con el que se esta trabajando*/
            SaveFileDialog saveFile2 = new SaveFileDialog();//Se crea una variable para el dialogo
            saveFile2.DefaultExt = "*.gg";//Se declara el tipo de archivo por defecto
            saveFile2.Filter = "Archivos (.*gg)|*.gg|Archivos (.*cpp)|*.cpp|Todos los archivos (*.*)|*.*";//Flitro de busqueda
            saveFile2.Title = "Guardar por primera vez";//Titulo del dialogo
            if (variables.guardarp == 1)//Si se guarda por primera vez
            {
                if (saveFile2.ShowDialog() == DialogResult.OK && saveFile2.FileName.Length > 0)//Si presiono guardar
                {
                    StreamWriter savep = File.CreateText(saveFile2.FileName);//se crea una variable para crear un archivo
                    savep.Write(codigo.Text);//Se incorpora el codigo hecho en el programa
                    savep.Flush();//Se refresca
                    savep.Close();//Se cierra
                    variables.direccion = saveFile2.FileName;//Se guarda la direccion elegida
                    variables.guardarp = 0;//Se modifica el guardado por primera vez
                    variables.guardar = 0;//Cambia el estado porque no se han echo cambios
                    variables.nombrea = Path.GetFileNameWithoutExtension(saveFile2.FileName);//Nombre del archivo
                    Form1.ActiveForm.Text = variables.nombrea + variables.nombre;//Titulo del programa
                }
            }
            else//Si ya estaba guardado
            {
                StreamWriter savep = File.CreateText(variables.direccion);//Crea el archivo con la direccion existente
                savep.Write(codigo.Text);//Se incorpora el codigo
                savep.Flush();//Se refresca
                savep.Close();//Se cierra
                variables.guardar = 0;//Cambia el estado porque no se han echo cambios
            }
        }

        private void guardarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile1 = new SaveFileDialog();//Se crea una variable para el dialogo
            saveFile1.DefaultExt = "*.gg";//Se declara el tipo de archivo por defecto
            saveFile1.Filter = "Archivos (.*gg)|*.gg|Archivos (.*cpp)|*.cpp|Todos los archivos (*.*)|*.*";//Flitro de busqueda
            saveFile1.Title = "Guardar Como";//Titulo del dialogo

            if (saveFile1.ShowDialog() == DialogResult.OK && saveFile1.FileName.Length > 0)//Si presiono guardar
            {
                StreamWriter save = File.CreateText(saveFile1.FileName);//se crea una variable para crear un archivo
                save.Write(codigo.Text);//Se incorpora el codigo hecho en el programa
                save.Flush();//Se refresca
                save.Close();//Se cierra
            }
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codigo.Visible = false;
            codigo.Clear();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1.ActiveForm.Close();
        }

        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Permite copiar texto del codigo si se selecciona*/
            if (codigo.SelectedText.Length > 0)//Si la seleccion es mayor de 0
            {
                codigo.Copy(); ;//Copia el texto selecionado
            }
            codigo.Select(codigo.Text.Length, 0);//Pocisiona el cursor al ultimo
            codigo.Focus();//Selecciona el textBox
        }

        private void cortarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Permite cortar texto del codigo si se selecciona*/
            if (codigo.SelectedText.Length > 0)//Si la seleccion es mayor de 0
            {
                codigo.Cut();//Corta el texto selecionado
            }
            codigo.Focus();//Selecciona el textBox
            codigo.Select(codigo.Text.Length, 0);//Pocisiona el cursor al ultimo
        }

        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Permite pegar texto del codigo si se selecciona*/
            var pos = Clipboard.ContainsText();//Almacena en la variable un true o false si contiene texto el sistema
            if (pos)//Compara si es true
            {
                codigo.Paste();//Pega el codigo
            }
            codigo.Focus();//Selecciona el textBox
            codigo.Select(codigo.Text.Length, 0);//Pocisiona el cursor al ultimo
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
           
            if (variables.guardar == 1)//Este if comprueba la variabe guardar 1=guardar 0=guardado
            {
                var resultado = MessageBox.Show("Desea guardar cambios en " + variables.nombrea, "IDE",
                    MessageBoxButtons.YesNoCancel);//Mensaje para guardar cambios
                if (resultado == DialogResult.Yes)//Si presiona "SI"
                {
                    SaveFileDialog saveFile2 = new SaveFileDialog();//Se crea una variable para el dialogo
                    saveFile2.DefaultExt = "*.gg";//Se declara el tipo de archivo por defecto
                    saveFile2.Filter = "Archivos (.*gg)|*.gg|Archivos (.*cpp)|*.cpp|Todos los archivos (*.*)|*.*";//Flitro de busqueda
                    saveFile2.Title = "Guardar";//Titulo del dialogo
                    var sresulatado = saveFile2.ShowDialog();//Se  declara una variable para saber el resultado
                    if (variables.guardarp == 1)//Si se guarda por primera vez
                    {
                        if (sresulatado == DialogResult.OK && saveFile2.FileName.Length > 0)//Si presiono guardar
                        {
                            StreamWriter savep = File.CreateText(saveFile2.FileName);//se crea una variable para crear un archivo
                            savep.Write(codigo.Text);//Se incorpora el codigo hecho en el programa
                            savep.Flush();//Se refresca
                            savep.Close();//Se cierra el archivo
                            variables.direccion = saveFile2.FileName;//Se guarda la direccion elegida
                            variables.guardarp = 0;//Se modifica el guardado por primera vez
                            e.Cancel = false;//No se cancela y se cierra el programa
                        }
                        if (sresulatado == DialogResult.Cancel)//Si preciono cancelar
                        {
                            e.Cancel = true;//Se cancela y no se cierra
                        }
                        if (resultado == DialogResult.No)//Si preciono no
                        {
                            e.Cancel = false;//No se cancela y se cierra
                        }
                    }
                    else//Si ya habia guardado por primera vez o abierto el archivo
                    {
                        StreamWriter savep = File.CreateText(variables.direccion);//Crea el archivo con la direccion existente
                        savep.Write(codigo.Text);//Se incorpora el codigo
                        savep.Flush();//Se refresca
                        savep.Close();//Se cierra
                        e.Cancel = false;//No se cancela y se cierra
                    }
                }
                if (resultado == DialogResult.No)//Si preciona NO
                {
                    e.Cancel = false;//No se cancela y se cierra
                }
                if (resultado == DialogResult.Cancel)//Si preciona cancelar
                {
                    e.Cancel = true;//Se cancela y no se cierra
                }
            }
        }

        private void codigo_TextChanged(object sender, EventArgs e)
        {
            /*Si se altera el codigo eso significa que se debe de guardar forzosamente*/
            variables.guardar = 1;//Se modifica el estado de guardar a 1
        }

        private void seleccionarTodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Selecciona todo el codigo del textBox o codigo*/
            codigo.Select(0, codigo.Text.Length);//Se establece la seleccion desde la pocicion 0 hasta el tamaño total
        }

        private void regresarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*Permite regresar el textBox si exite regreso*/
            if (codigo.CanUndo)//Si se puede regresar
            {
                codigo.Undo();//Regresa a lo anterior
                codigo.ClearUndo();//Borra lo que estaba
            }
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Acerca_d frm = new Acerca_d();
            frm.Show();
        }

        private void shellToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("Shell.exe");
        }

        private void ayudaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process.Start("Menu.chm");
        }

        private void compilarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamWriter save = new StreamWriter("boob.gg");//se crea una variable para crear un archivo
            save.Write(codigo.Text);//Se incorpora el codigo hecho en el programa
            save.Flush();//Se refresca
            save.Close();//Se cierra
            /*Cuando uno preciona Compilar se abre el compilador*/
            Process.Start("Compilador.exe");//Se ejecuta el interprete de comandos
            System.Threading.Thread.Sleep(5000);
        }
    }
}
